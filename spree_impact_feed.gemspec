# encoding: UTF-8
lib = File.expand_path('../lib/', __FILE__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'spree_impact_feed/version'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_impact_feed'
  s.version     = SpreeImpactFeed.version
  s.summary     = 'Generate product feed for Impact'
  s.description = ''
  s.required_ruby_version = '>= 2.6'

  s.author    = 'Duc T Lam'
  s.email     = 'dlam@naturesflavors.com'
  s.homepage  = 'https://github.com/ducl_13/spree_impact_feed'
  s.license = 'BSD-3-Clause'

  s.files       = `git ls-files`.split("\n").reject { |f| f.match(/^spec/) && !f.match(/^spec\/fixtures/) }
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree', '>= 3.7'
  # s.add_dependency 'spree_backend' # uncomment to include Admin Panel changes
  s.add_dependency 'spree_extension'

  s.add_development_dependency 'spree_dev_tools'
end
